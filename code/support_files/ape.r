#library(seqinr, lib.loc="/users/jmacrand")
require('ggplot2')
require('ggtreqe', lib.loc="/users/jmacrand")
require("gridExtra")
require('seqinr')
require('ape')

myArgs <- commandArgs(trailingOnly = TRUE)

alignment=read.alignment(myArgs[1], format="fasta")

rootedNJtree <- function(alignment)
{
  
  makemytree <- function(alignmentmat)
  {
    mydist <- dist.alignment(alignment)
    
    mytree <- njs(mydist)
    mytree <- makeLabel(mytree, space="") # get rid of spaces in tip names.
    
  }
  
  mymat  <- as.matrix.alignment(alignment)
  myrootedtree <- makemytree(mymat)
  pdf(myArgs[2])

  plot.phylo(myrootedtree, type="p", show.tip.label = TRUE, show.node.label = TRUE, tip.color = "black", font=3)  # plot the rooted phylogenetic tree

  return(myrootedtree)

  dev.off()
}

finaltree <- rootedNJtree(alignment)

write.tree(finaltree, myArgs[2])
